import csv
import json
from datetime import datetime
import math
from operator import itemgetter

#TRIPS_FILE = '/dat/divvy/Divvy_Stations_Trips_2013/trips_head_1000.csv'
#TRIPS_FILE = '/dat/divvy/Divvy_Stations_Trips_2013/Divvy_Trips_2013.csv'
TRIPS_FILE = '/dat/divvy/Divvy_Stations_Trips_2013/Divvy_Trips_2013_no_outliers.csv'

STATIONS_FILE = '/dat/divvy/Divvy_Stations_Trips_2013/Divvy_Stations_2013.csv'

NUM_NEAREST_NEIGHBORS = 5

def load_stations():
  sf = open(STATIONS_FILE, 'rb')
  station_reader = csv.reader(sf)
  stations = dict()
  row_idx = 0
  for row in station_reader:
    if row_idx == 0:
      field_names = row
    else:
      col_idx = 0
      station = dict()
      for col in row:
        station[field_names[col_idx]] = row[col_idx]
        col_idx += 1
      stations[station['id']] = station
    row_idx += 1
  return stations

def load_trip_day_counts():
  """
  returns dict of {
    <date_str>: <trip count>     
    }
  """
  tf = open(TRIPS_FILE, 'rb')
  trips_reader = csv.reader(tf)
  day_counts = dict()

  first_row = True
  for row in trips_reader:
    if first_row:
      field_names = row
      first_row = False
    else:
      col_idx = 0
      trip = dict()
      for col in row:
        trip[field_names[col_idx]] = row[col_idx]
        col_idx += 1
      datetime_str = trip['starttime']
      date_str = datetime_str.split(' ')[0]

      if not date_str in day_counts:
        day_counts[date_str] = 0
      day_counts[date_str] += 1
  return day_counts


def load_trip_counts(day_counts):
  """
  returns dict of {
    <station_id>: {
      'arrivals': <count>
      'departures': <count>
      }
  }
  """
  tf = open(TRIPS_FILE, 'rb')
  trips_reader = csv.reader(tf)
  trip_counts = dict()

  first_row = True
  for row in trips_reader:
    if first_row:
      field_names = row
      first_row = False
    else:
      col_idx = 0
      trip = dict()
      for col in row:
        trip[field_names[col_idx]] = row[col_idx]
        col_idx += 1
      departure_id = trip['from_station_id']
      arrival_id = trip['to_station_id']
      datetime_str = trip['starttime']
      date_str = datetime_str.split(' ')[0]
      trip_weight = 1.0 / float(day_counts[date_str])
      if not departure_id in trip_counts:
        trip_counts[departure_id] = dict()
        trip_counts[departure_id]['raw_arrivals'] = 0
        trip_counts[departure_id]['raw_departures'] = 0
      if not arrival_id in trip_counts:
        trip_counts[arrival_id] = dict()
        trip_counts[arrival_id]['raw_arrivals'] = 0
        trip_counts[arrival_id]['raw_departures'] = 0

      trip_counts[departure_id]['raw_departures'] += trip_weight
      trip_counts[arrival_id]['raw_arrivals'] += trip_weight
  return trip_counts

def pretty_print(jmap):
  print(json.dumps(jmap, indent=4))

def join_station_trip_counts(stations, trip_counts):
  """
  adds the counts from trip-counts to each station's entry, keyed
  by station id.
  """

  #make sure something will always be there
  for station_id in stations:
    stations[station_id]['raw_arrivals'] = 0
    stations[station_id]['raw_departures'] = 0

  #override the zeros if we have data
  for station_id in trip_counts:
    deps = trip_counts[station_id]['raw_departures']
    stations[station_id]['raw_departures'] = deps
    arrs = trip_counts[station_id]['raw_arrivals']
    stations[station_id]['raw_arrivals'] = arrs
  return 

def compute_days_active(station_counts):
  """
  computes the number of days between the station's 'online date'
  and dec 31, 2013. adds a new field called 'days_online'
  """
  date_format = "%m/%d/%Y"
  ref_date = datetime.strptime('12/31/2013', date_format)

  for station_id in station_counts:
    open_date_str = station_counts[station_id]['online date']
    open_date = datetime.strptime(open_date_str, date_format)
    delta = ref_date - open_date
    num_days = delta.days
    station_counts[station_id]['days_online'] = num_days
  return

def compute_distance(lat1, lng1, lat2, lng2):
  """
  pretending lat/long are rectangular coordinates. it's not
  that bad of an approximation for what we're doing (finding
  k nearest neighbors, and getting a relative weighting)
  """
  lt_diff = lat2 - lat1
  lt_sqr =  lt_diff * lt_diff
  lg_diff = lng2 - lng1
  lg_sqr = lg_diff * lg_diff
  distance = math.sqrt(lg_sqr + lt_sqr)
  return distance

  
def find_all_distances(station_id, stations):
  """
  returns a list of dicts:
    [
      { station_id = <station_id>,
        distance = <distance>
      }
      ]
  """
  neighbors = list()
  lat = float(stations[station_id]['latitude'])
  lng = float(stations[station_id]['longitude'])
  for other_station_id in stations:
    if other_station_id is not station_id:
      other_lat = float(stations[other_station_id]['latitude'])
      other_lng = float(stations[other_station_id]['longitude'])
      distance = compute_distance(lat, lng, other_lat, other_lng)
      neighbor = dict()
      neighbor['neighbor_station_id'] = other_station_id
      neighbor['distance'] = distance
      neighbor['neighbor_name'] = stations[other_station_id]['name']
      neighbors.append(neighbor)
  return neighbors

def find_nearest_neighbors(stations, num_neighbors):
  for station_id in stations:
    neighbors = find_all_distances(station_id, stations)
    neighbors = sorted(neighbors, key=itemgetter('distance'))
    top_neighbors = neighbors[:num_neighbors]
    stations[station_id]['nearest_neighbors'] = top_neighbors
  return

def compute_normalized_trip_counts(stations):
  """
  computes a new field that is the number of arrivals or departures
  divided by the number of days online
  """
  for station_id in stations:
    num_days = float(stations[station_id]['days_online'])
    raw_arr = float(stations[station_id]['raw_arrivals'])
    raw_dep = float(stations[station_id]['raw_departures'])
    norm_arr = raw_arr / num_days
    norm_dep = raw_dep / num_days
    #print("num_days[" + str(num_days) + "] raw_arr[" + str(raw_arr) + "] norm_arr[" + str(norm_arr) + "]")
    stations[station_id]['norm_arrivals'] = norm_arr
    stations[station_id]['norm_departures'] = norm_dep
  return

def compute_expected_trip_counts(stations):
  """
  gives every station a new field that is the expected arrivals and
  departure counts, which is computed as the weighted average of
  the coutns of the nearest neighbors (weighted by inverse distance).
  """
  for station_id in stations:
    weighted_arrival_tally = 0.0
    weighted_departure_tally = 0.0
    neighbor_distance_tally = 0.0
    for neighbor in stations[station_id]['nearest_neighbors']:
      distance = neighbor['distance']
      inv_distance = 1.0 / distance
      neighbor_id = neighbor['neighbor_station_id']
      neighbor_arrs = stations[neighbor_id]['norm_arrivals']
      neighbor_deps = stations[neighbor_id]['norm_departures']
      weighted_arrival_tally += neighbor_arrs * inv_distance
      weighted_departure_tally += neighbor_deps * inv_distance
      neighbor_distance_tally += inv_distance
    expected_arrs = weighted_arrival_tally  / neighbor_distance_tally
    expected_deps = weighted_departure_tally  / neighbor_distance_tally
    stations[station_id]['expected_arrivals'] = expected_arrs
    stations[station_id]['expected_departures'] = expected_deps
  return


def compute_unexpected(expected_count, observed_count):
  if expected_count == 0.0:
    return 0.0
  diff = float(observed_count) - expected_count
  frac = diff / expected_count
  return frac

def compute_percent_unexpected(stations):
  """
  computes the percent diff between teh normalized counts and
  the expected normalized counts
  """
  for station_id in stations:
    station = stations[station_id]
    exp_arrivals = station['expected_arrivals']
    obs_arrivals = station['norm_arrivals']
    unexpected_arr = compute_unexpected(exp_arrivals, obs_arrivals)
    station['frac_unexpected_arrivals'] = unexpected_arr

    exp_departures = station['expected_departures']
    obs_departures = station['norm_departures']
    unexpected_dep = compute_unexpected(exp_departures, obs_departures)
    station['frac_unexpected_departures'] = unexpected_dep
  return

def main():

  stations = load_stations()
  day_counts = load_trip_day_counts()
  pretty_print(day_counts)
  trip_counts = load_trip_counts(day_counts)

  join_station_trip_counts(stations, trip_counts)
  compute_days_active(stations)
  find_nearest_neighbors(stations, NUM_NEAREST_NEIGHBORS)
  compute_normalized_trip_counts(stations)

  compute_expected_trip_counts(stations)
  
  compute_percent_unexpected(stations)
  pretty_print(stations)
  return

main()
